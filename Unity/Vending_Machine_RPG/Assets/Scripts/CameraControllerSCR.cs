﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControllerSCR : MonoBehaviour {
	public Transform targetPoint, pivotPoint;
	public Vector3 offsetPos;
	public bool useOffsetValues;
	public float rotateSpeed, maxViewAngle, minViewAngle;

	public bool invertY;
 

	// Use this for initialization
	void Start () {
		Cursor.lockState = CursorLockMode.Locked;
		if (!useOffsetValues) {
			
			//set camera position away from player
			offsetPos = targetPoint.position - transform.position;
		}
		pivotPoint.transform.position = targetPoint.transform.position;
		pivotPoint.transform.parent = targetPoint.transform;
		}

	
	// Update is called once per frame
	void LateUpdate () {

		//get x position of the mouse & rotate to target
		float horizontal = Input.GetAxis("Mouse X") * rotateSpeed;
		targetPoint.Rotate (0, horizontal, 0);

		float vertical = Input.GetAxis ("Mouse Y") * rotateSpeed;
		//invert y axis
		if (invertY) {
			pivotPoint.Rotate (vertical, 0, 0);	
		} else {
			pivotPoint.Rotate (-vertical, 0, 0);
		}

		//Limit up/down camera rotation
		if(pivotPoint.rotation.eulerAngles.x > maxViewAngle && pivotPoint.rotation.eulerAngles.x < 180f) {
			pivotPoint.rotation = Quaternion.Euler (maxViewAngle, 0, 0);
		}

		if (pivotPoint.rotation.eulerAngles.x > 180 && pivotPoint.rotation.eulerAngles.x < 360 + minViewAngle) {
		
			pivotPoint.rotation = Quaternion.Euler (360f + minViewAngle, 0, 0);
		}

		//move the camera based on the current rotation of the target & the original offset
		float desiredYAngle = targetPoint.eulerAngles.y;
		float desiredXAngle = pivotPoint.eulerAngles.x;

		Quaternion rotation = Quaternion.Euler (desiredXAngle, desiredYAngle, 0);
		transform.position = targetPoint.position - (rotation * offsetPos);
		//track camera to player position
		//transform.position = targetPoint.position - offsetPos;
		//have camera look at player

		if (transform.position.y < targetPoint.position.y) {
		
			transform.position = new Vector3(transform.position.x, targetPoint.position.y -0.5f, transform.position.z);
		}
		transform.LookAt (targetPoint);
	}
}
