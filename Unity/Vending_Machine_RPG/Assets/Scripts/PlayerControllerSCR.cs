﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerSCR : MonoBehaviour {
	public float moveSpeed, jumpForce, gravityScale;
	public CharacterController controller;
	private Vector3 moveDirection;

	// Use this for initialization
	void Start () {
		controller = GetComponent<CharacterController> ();
	}
	
	// Update is called once per frame
	void Update () {
		//allow for movement in the world
		//moveDirection = new Vector3 (Input.GetAxis ("Horizontal") * moveSpeed, moveDirection.y, Input.GetAxis ("Vertical") * moveSpeed);
		float yStore = moveDirection.y;
		moveDirection = (transform.forward * Input.GetAxis("Vertical")) + (transform.right * Input.GetAxis("Horizontal"));
		moveDirection = moveDirection.normalized * moveSpeed;
		moveDirection.y = yStore;
		//check if chara is on ground: if yes, allow jump
		if (controller.isGrounded) {
			moveDirection.y = 0f;
			//allow for jumping
			if (Input.GetButtonDown ("Jump")) {
		
				moveDirection.y = jumpForce;
			}
		}

		moveDirection.y = moveDirection.y + (Physics.gravity.y * gravityScale * Time.deltaTime);
		////ensure movement inputs are not tied to framerate
		controller.Move (moveDirection * Time.deltaTime);
	}
}
